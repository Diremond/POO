package S03TP2;

import java.time.LocalDate;
import java.time.Period;
import javax.swing.JOptionPane;
import java.util.Random;

public class EssaiDate {

	//public static void main(String[] args) {
	public void temp(){
		
		LocalDate date1;
		LocalDate date2;
		LocalDate date3;
		Period p;
		int nbdays2Periods=0;
		Random alea = new Random();
		
		int jour;
		int mois;
		int annee;
		LocalDate dateTemp;
		
		//Date d'aujourd'hui
		JOptionPane.showMessageDialog( null,"Voici la date d'aujourd'hui : "+LocalDate.now());
		
		//Saisie d'une date 1
		date1 = Saisie.date();
		JOptionPane.showMessageDialog(null,"Vous avez saisi la date 1 : "+date1);
		
		//Saisie d'une date 2
		date2 = Saisie.date();
		JOptionPane.showMessageDialog(null,"Vous avez saisi la date 2 : "+date2);
		
		//JOptionPane.showMessageDialog(null, "Temps entre ces deux dates : "+date1.until(date2));
		p = Period.between(date1, date2);
		nbdays2Periods = p.getDays()+p.getMonths()*30+p.getYears()*365;
		JOptionPane.showMessageDialog(null, "Nombre de jours entre ces deux dates : "+nbdays2Periods);
		
		//G�n�ration d'une 3�me date
		do{
			jour = alea.nextInt(31);
			mois = alea.nextInt(12);
			annee = alea.nextInt(10000);
			dateTemp = LocalDate.of(1998,Integer.valueOf(mois),1);
		}while(Integer.valueOf(jour) > dateTemp.lengthOfMonth() || Integer.valueOf(jour) < 1 || Integer.valueOf(mois) < 1 || Integer.valueOf(mois) > 12);
		
		date3 = LocalDate.of(annee, mois, jour);
		
		JOptionPane.showMessageDialog(null, "3�me date g�n�r�e : "+date3);
	}
}
