package S03TP2;
import javax.swing.JOptionPane;

public class EssaiSaisie {
	//public static void main(String[] args) {
	public void temp(String[] args) {
		int valeurRetenue = 0;
		int min = 5;
		int max = 10;
		Saisie entree = new Saisie();
		do{
			valeurRetenue = entree.controle(min,max);
		}while(valeurRetenue<min || valeurRetenue>max);
		
		JOptionPane.showMessageDialog(null, "Vous avez saisi : "+valeurRetenue);
	}
}