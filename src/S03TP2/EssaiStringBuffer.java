package S03TP2;

public class EssaiStringBuffer {

	public static void main(String[] args) {
		StringBuffer chaine = new StringBuffer("Paramètres : ");
		
		for(int cpt=0; cpt<args.length; cpt++){
			chaine.insert(chaine.length(),args[cpt]+" ");
		}
		System.out.println(chaine);
		
		for(int cpt=0; cpt<chaine.length(); cpt++){
			if(chaine.charAt(cpt) == '.'){
				chaine.deleteCharAt(cpt);
			}else if(chaine.charAt(cpt)>='0' && chaine.charAt(cpt)<='9'){
				chaine.replace(cpt, cpt+1, "#");
			}else if(chaine.charAt(cpt)=='A' && cpt != 0){
				chaine.insert(cpt+1, '-');
			}
		}
		
		System.out.println(chaine);
	}

}