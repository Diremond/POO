package S03TP2;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class EssaiDateTimeFormatter {
	//public static void main(String[] args){
	public void temp(){
		  
		  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy[ HH:mm]");
		  LocalDate date = LocalDate.now();
		  LocalTime time = LocalTime.of(9, 30, 07);
		  LocalDateTime dateTimeF = LocalDateTime.of(date, time);
		  
		  String text = formatter.format(dateTimeF);
		  
		  System.out.println("Format par d�faut : "+text);
		  
		  formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		  System.out.println(formatter.format(dateTimeF));
		  
		  formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
		  System.out.println(formatter.format(dateTimeF));
		  
		  ZoneId localisation = ZoneId.of("Europe/Paris");
		  ZonedDateTime zDateTimeF = ZonedDateTime.of(date, time, localisation);
		  
		  formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy zzzz");
		  System.out.println(formatter.format(zDateTimeF));
		  
		  formatter = DateTimeFormatter.ofPattern("MMMM dd HH:mm:ss zzzz yyyy");
		  System.out.println(formatter.format(zDateTimeF));
		  
		  formatter = DateTimeFormatter.ofPattern("E, dd MMM yyyy HH:mm:ss z");
		  System.out.println(formatter.format(zDateTimeF));
		  
		  System.out.println("Styles pr�d�finis :");
		  
		  formatter = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT);
		  System.out.println(formatter.format(zDateTimeF));
		  
		  formatter = DateTimeFormatter.ofLocalizedTime(FormatStyle.MEDIUM);
		  System.out.println(formatter.format(zDateTimeF));
		  
		  formatter = DateTimeFormatter.ofLocalizedTime(FormatStyle.LONG);
		  System.out.println(formatter.format(zDateTimeF));
		  
		  formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);
		  System.out.println(formatter.format(zDateTimeF));
		  
		  formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM, FormatStyle.SHORT);
		  System.out.println(formatter.format(zDateTimeF));
		  
		  formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG, FormatStyle.LONG);
		  System.out.println(formatter.format(zDateTimeF));
	}
}