package S03TP2;

import javax.swing.JOptionPane;

public class SaisieFraction {

	//public static void main(String[] args) {
	public void temp(String[] args){
		//Déclaration des variables locales
		int min;
		int max;
		String inputNum ="";
		String inputDeno="";
		int num;
		int deno;
		
		if(args.length == 2 && Integer.valueOf(args[0])<Integer.valueOf(args[1])){
			//Transformation des variables
			min = Integer.valueOf(args[0]);
			max = Integer.valueOf(args[1]);
			
			//Vérification entre min et max
			do{
				inputNum = JOptionPane.showInputDialog("Veuillez entrer un numérateur :");
				inputDeno = JOptionPane.showInputDialog("Veuillez entrer un dénominateur :");
				num = Integer.valueOf(inputNum);
				deno = Integer.valueOf(inputDeno);
			}while(num < min || num > max || deno < min || deno > max);
			System.out.println(num/deno);
		}else{
			System.out.println("Une erreur s'est produite.");
		}
	}
}