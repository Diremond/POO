package S03TP2;
import java.time.LocalDate;

import javax.swing.JOptionPane;

public class Saisie {
	int controle(int min, int max){
		String nbr = "";
		while(nbr.matches("[0-9][0-9]*") == false){
			nbr = JOptionPane.showInputDialog( null, "Veuillez saisir votre chiffre", "Chiffre");	
		}
		return Integer.valueOf(nbr);
	}
	
	static LocalDate date(){
		String jour = "";
		String mois = "";
		String annee = "";
		LocalDate date;
		LocalDate dateTemp;
		
		do{
			jour = JOptionPane.showInputDialog(null, "Veuillez saisir le jour : ");
			do{
				mois = JOptionPane.showInputDialog(null, "Veuillez saisir le mois : ");
			}while(Integer.valueOf(mois) < 1 || Integer.valueOf(mois) > 12);
			
			annee = JOptionPane.showInputDialog(null, "Veuillez saisir l'ann�e : ");
			dateTemp = LocalDate.of(1998,Integer.valueOf(mois),1);
			
		}while(Integer.valueOf(jour) > dateTemp.lengthOfMonth() || Integer.valueOf(jour) < 1 || Integer.valueOf(mois) < 1 || Integer.valueOf(mois) > 12);
		
		date = LocalDate.of(Integer.valueOf(annee),Integer.valueOf(mois),Integer.valueOf(jour));
		
		return date;
	}
}