package S03TP2;

public class EssaiString {

	public static void main(String[] args) {
		String parametres="";
		for(int cpt =0; cpt < args.length; cpt++){
			parametres += args[cpt];
			if(cpt != args.length-1){
				parametres +=";";
			}
		}
		//Affichage paramètres bruts
		System.out.println(parametres);
		
		//Affichage paramètres minus
		System.out.println(parametres.toLowerCase());
		
		//Affichage de la longueur de la chaine
		System.out.println("Longueur"+parametres.length());
		
		//Affichage paramètres qui contiennent un "."
		for(int cpt=0; cpt < args.length; cpt++){
			if(args[cpt].contains("."))
				System.out.print(args[cpt]+" ");
		}
		//Affichage paramètres qui contiennent un chiffre
		for(int cpt=0; cpt<args.length; cpt++){
			if(args[cpt].matches(".*[0-9].*"))
				System.out.print("pp"+args[cpt]+" ");
		}
	}
}
